using System.Collections.Generic;
using Niceplay.TestTask.Core;
using Niceplay.TestTask.Core.Models;
using Niceplay.TestTask.Core.Settings;
using NUnit.Framework;
using static Niceplay.TestTask.Core.Models.DishNameSubCondition;
using static Niceplay.TestTask.Core.Settings.IngredientData;

namespace Niceplay.TestTask.Tests
{
    internal class DishNamingTests
    {
        [Test]
        public void TestConditions()
        {
            var ingredientsSet1 = new List<IngredientKind>
            {
                IngredientKind.Carrot,
                IngredientKind.Carrot,
                IngredientKind.Carrot,
                IngredientKind.Carrot,
                IngredientKind.Carrot,
            };

            var ingredientsSet2 = new List<IngredientKind>
            {
                IngredientKind.Carrot,
                IngredientKind.Meat,
                IngredientKind.Onion,
                IngredientKind.Pepper,
                IngredientKind.Potato,
            };

            var ingredientsSet3 = new List<IngredientKind>
            {
                IngredientKind.Meat,
                IngredientKind.Meat,
                IngredientKind.Meat,
                IngredientKind.Pepper,
                IngredientKind.Pepper,
            };

            var condition1 = new DishNameCondition(
                new List<DishNameSubCondition>
                {
                    new DishNameSubCondition(IngredientKind.Carrot, EqualityOperation.Eq, 5, BooleanOperation.And)
                },
                ""
            );

            var condition2 = new DishNameCondition(
                new List<DishNameSubCondition>
                {
                    new DishNameSubCondition(IngredientKind.Meat, EqualityOperation.Gt, 3, BooleanOperation.And)
                },
                ""
            );

            var condition3 = new DishNameCondition(
                new List<DishNameSubCondition>
                {
                    new DishNameSubCondition(IngredientKind.Meat, EqualityOperation.Gte, 3, BooleanOperation.And)
                },
                ""
            );

            Assert.IsTrue(condition1.IsMet(ingredientsSet1));
            Assert.IsFalse(condition1.IsMet(ingredientsSet2));
            Assert.IsFalse(condition1.IsMet(ingredientsSet3));

            Assert.IsFalse(condition2.IsMet(ingredientsSet1));
            Assert.IsFalse(condition2.IsMet(ingredientsSet2));
            Assert.IsFalse(condition2.IsMet(ingredientsSet3));

            Assert.IsFalse(condition3.IsMet(ingredientsSet1));
            Assert.IsFalse(condition3.IsMet(ingredientsSet2));
            Assert.IsTrue(condition3.IsMet(ingredientsSet3));
        }
    }
}
