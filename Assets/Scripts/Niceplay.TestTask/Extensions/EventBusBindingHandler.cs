using System.Collections.Generic;
using Niceplay.TestTask.EventBusSystem;
using UnityEngine;

namespace Niceplay.TestTask.Extensions
{
    public class EventBusBindingHandler : MonoBehaviour
    {
        private readonly List<(EventBus.Subscription s, Component c)> _subscriptionTuples
            = new List<(EventBus.Subscription s, Component c)>(10);

        public void Add(EventBus.Subscription subscription, Component component)
        {
            if (subscription != null && !subscription.IsFreed)
                _subscriptionTuples.Add((subscription, component));
        }

        private void Update()
        {
            for (var i = 0; i < _subscriptionTuples.Count; i++)
            {
                var (s, c) = _subscriptionTuples[i];

                if (c.Value() == null || s == null || s.IsFreed)
                {
                    _subscriptionTuples.RemoveAt(i);
                    EventBus.Unsubscribe(s);
                }
            }
        }
    }
}
