using Niceplay.TestTask.EventBusSystem;
using UnityEngine;

namespace Niceplay.TestTask.Extensions
{
    public static class EventBusExtensions
    {
        private static readonly EventBusBindingHandler BindingHandler
            = new GameObject("EventBus_BindingHandler").AddComponent<EventBusBindingHandler>();

        public static void AddTo<T>(this EventBus.Subscription subscription, T component) where T : Component
        {
            BindingHandler.Add(subscription, component);
        }
    }
}
