using UnityEngine;

namespace Niceplay.TestTask.Extensions
{
    public static class ComponentExtensions
    {
        public static T Value<T>(this T component) where T : Component
        {
            return component ? component : null;
        }

        public static GameObject Value(this GameObject component)
        {
            return component ? component : null;
        }
    }
}
