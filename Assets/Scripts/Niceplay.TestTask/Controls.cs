using UnityEngine;

namespace Niceplay.TestTask
{
    public static class Controls
    {
        public const KeyCode RestartGame = KeyCode.R;
        public const KeyCode PrintAllPossibleDishes = KeyCode.T;
    }
}
