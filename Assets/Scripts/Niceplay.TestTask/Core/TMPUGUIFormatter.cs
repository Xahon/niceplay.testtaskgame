﻿using TMPro;
using UnityEngine;

namespace Niceplay.TestTask.Core
{
    [ExecuteAlways]
    public class TMPUGUIFormatter : MonoBehaviour
    {
        [SerializeField]
        [Multiline]
        private string format;

        [SerializeField]
        private TextMeshProUGUI tmp;

        public void SetValues(params object[] args)
        {
            var formattedString = string.Format(format, args);
            tmp.SetText(formattedString);
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            tmp.SetText(format);
        }
#endif
    }
}
