﻿using System.Collections.Generic;
using UnityEngine;

namespace Niceplay.TestTask.Core
{
    public class IngredientSpawners : MonoBehaviour
    {
        [SerializeField]
        private List<IngredientSpawner> spawners;

        public void Release(GameObject @object)
        {
            foreach (var spawner in spawners)
            {
                if (spawner.Pool.Release(@object))
                    return;
            }

            Destroy(@object);
        }
    }
}
