﻿using Niceplay.TestTask.Core.Events.Requests;
using Niceplay.TestTask.EventBusSystem;
using UnityEngine;

namespace Niceplay.TestTask.Core.Controllers
{
    public class RestartController : MonoBehaviour
    {
        private void Update()
        {
            if (Input.GetKeyDown(Controls.RestartGame))
                RestartGame();
        }

        public void RestartGame()
        {
            Debug.Log("Restarting game");
            var ingredients = FindObjectsOfType<Ingredient>();
            foreach (var ingredient in ingredients)
            {
                Destroy(ingredient.gameObject);
            }

            EventBus.Publish(new ResetScoreRequest());
        }
    }
}
