﻿using Niceplay.TestTask.Core.Events;
using Niceplay.TestTask.Core.Events.Requests;
using Niceplay.TestTask.Core.Models;
using Niceplay.TestTask.Core.Settings;
using Niceplay.TestTask.EventBusSystem;
using Niceplay.TestTask.Extensions;
using UnityEngine;

namespace Niceplay.TestTask.Core.Controllers
{
    public class DishUITextController : BaseController
    {
        [SerializeField]
        private DishNamingPreferences namingPreferences;

        [SerializeField]
        private TMPUGUIFormatter dishTextFormatter;

        protected override void Awake()
        {
            base.Awake();
            EventBus.Subscribe<DishCooked>(OnDishCookedEvent).AddTo(this);
            EventBus.Subscribe<ResetScoreRequest>(_ =>
                    SetDishNames(GameStateAccess.GameState.lastDish, GameStateAccess.GameState.bestDish)
                )
                .AddTo(this);
        }

        protected override void OnStartController(StartControllersRequest request)
        {
            SetDishNames(GameStateAccess.GameState.lastDish, GameStateAccess.GameState.bestDish);
        }

        private void OnDishCookedEvent(DishCooked @event)
        {
            var newDishName = namingPreferences.GetDishName(@event.Dish);
            Debug.Log($"You cooked \"{newDishName}\"");
            SetDishNames(GameStateAccess.GameState.lastDish, GameStateAccess.GameState.bestDish);
        }

        private void SetDishNames(Dish lastDish, Dish bestDish)
        {
            dishTextFormatter.SetValues(
                namingPreferences.GetDishName(lastDish),
                namingPreferences.GetDishIngredientsString(lastDish),
                namingPreferences.GetDishName(bestDish),
                namingPreferences.GetDishIngredientsString(bestDish)
            );
        }
    }
}
