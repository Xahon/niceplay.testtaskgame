using System.Collections.Generic;
using Niceplay.TestTask.Core.Events;
using Niceplay.TestTask.Core.Events.Requests;
using Niceplay.TestTask.Core.Models;
using Niceplay.TestTask.Core.Settings;
using Niceplay.TestTask.EventBusSystem;
using UnityEngine;

namespace Niceplay.TestTask.Core.Controllers
{
    public class PanIconsController : BaseController
    {
        [SerializeField]
        private List<SpriteRenderer> icons;

        [SerializeField]
        private Sprite noIngredientIcon;

        protected override void Awake()
        {
            base.Awake();
            EventBus.Subscribe<ScoreUpdated>(_ => SetIcons(GameStateAccess.GameState.currentDish.ingredients));
            EventBus.Subscribe<DishCooked>(_ => ClearAllIcons());
            EventBus.Subscribe<ResetScoreRequest>(_ => ClearAllIcons());
        }

        protected override void OnStartController(StartControllersRequest request)
        {
            ClearAllIcons();
            SetIcons(GameStateAccess.GameState.currentDish.ingredients);
        }

        private void SetIcons(IEnumerable<IngredientKind> ingredients)
        {
            var i = 0;
            foreach (var ingredient in ingredients)
            {
                icons[i].sprite = IngredientDataMapper.GetData(ingredient).Icon;
                ++i;
            }
        }

        private void ClearAllIcons()
        {
            foreach (var spriteRenderer in icons)
            {
                spriteRenderer.sprite = noIngredientIcon;
            }
        }
    }
}
