using Niceplay.TestTask.Core.Events;
using Niceplay.TestTask.Core.Events.Requests;
using Niceplay.TestTask.Core.Models;
using Niceplay.TestTask.EventBusSystem;
using UnityEngine;

namespace Niceplay.TestTask.Core.Controllers
{
    public class ScoreUIController : BaseController
    {
        [SerializeField]
        private TMPUGUIFormatter scoreTextFormatter;

        protected override void Awake()
        {
            base.Awake();
            EventBus.Subscribe<ScoreUpdated>(OnScoreIncreased);
        }

        protected override void OnStartController(StartControllersRequest request)
        {
            SetUIScore(GameStateAccess.GameState.TotalScore);
        }

        private void OnScoreIncreased(ScoreUpdated @event)
        {
            SetUIScore(@event.TotalGameScore);
        }

        private void SetUIScore(int totalGameScore)
        {
            scoreTextFormatter.SetValues(totalGameScore);
        }
    }
}
