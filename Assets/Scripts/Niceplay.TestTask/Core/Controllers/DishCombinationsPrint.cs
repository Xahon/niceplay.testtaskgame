﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Niceplay.TestTask.Core.Models;
using Niceplay.TestTask.Core.Settings;
using Niceplay.TestTask.Utils;
using UnityEngine;

namespace Niceplay.TestTask.Core.Controllers
{
    public class DishCombinationsPrint : MonoBehaviour
    {
        [SerializeField]
        private DishNamingPreferences namingPreferences;

        private void Update()
        {
            if (Input.GetKeyDown(Controls.PrintAllPossibleDishes))
                PrintAllPossibleDishes();
        }

        private void PrintAllPossibleDishes()
        {
            var path = Path.Combine(Application.dataPath, "AllDishes.txt");
            Debug.Log($"Writing combinations at: {path}");

            var allPossibleDishes = GetAllPermutations()
                .OrderByDescending(d => d.ScoreSummary.Score)
                .ToList();

            try
            {
                using (var fs = new StreamWriter(path))
                {
                    foreach (var dish in allPossibleDishes)
                    {
                        fs.Write(namingPreferences.GetDishName(dish));
                        fs.Write(" ");
                        fs.Write(namingPreferences.GetDishIngredientsString(dish));
                        fs.Write(" ");
                        fs.WriteLine($"[{dish.ScoreSummary.Score}]");
                    }

                    fs.Flush();
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                return;
            }

            Debug.Log($"Dish combinations written at: {path}");
        }

        private IEnumerable<Dish> GetAllPermutations()
        {
            var allKinds = typeof(IngredientKind)
                .GetEnumValues()
                .OfType<IngredientKind>()
                .ToList();

            return (from i1 in allKinds
                    from i2 in allKinds
                    from i3 in allKinds
                    from i4 in allKinds
                    from i5 in allKinds
                    select new Dish(new List<IngredientKind>
                    {
                        i1,
                        i2,
                        i3,
                        i4,
                        i5
                    })
                ).Distinct(
                    new EqualityComparerAdapter<Dish>(
                        (dish1, dish2) => dish1.GetHashCode() == dish2.GetHashCode()
                    )
                );
        }
    }
}
