using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Niceplay.TestTask.Core.Events;
using Niceplay.TestTask.Core.Events.Requests;
using Niceplay.TestTask.Core.Models;
using Niceplay.TestTask.Core.Settings;
using Niceplay.TestTask.EventBusSystem;
using Niceplay.TestTask.Extensions;
using Niceplay.TestTask.Utils;
using UnityEngine;

namespace Niceplay.TestTask.Core.Controllers
{
    public sealed class Startup : MonoBehaviour
    {
        [SerializeField]
        private IngredientSpawners spawners;

        private const string GameStateKey = nameof(Startup) + "_" + "GameState";


        private void Awake()
        {
            EventBus.Subscribe<IngredientAdded>(OnIngredientAddedEvent).AddTo(this);
            EventBus.Subscribe<ResetScoreRequest>(OnScoreResetRequest).AddTo(this);

            LoadDishesScoreFromJsonAndApply();
            LoadPreviousStateFromMemory();
        }

        private void Start()
        {
            EventBus.Publish(new StartControllersRequest());
            HandleFullPanIfIsFull();
        }

        private static void LoadDishesScoreFromJsonAndApply()
        {
            var path = Path.Combine(Application.dataPath, "ingredients.json");
            var dishScore = new DishScore();

            try
            {
                var json = File.ReadAllText(path, Encoding.UTF8);
                dishScore = JsonUtility.FromJson<DishScore>(json);
            }
            catch (FileNotFoundException e)
            {
                Debug.Log($"File '{path}' not found. Using default settings");
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                Debug.Log("Using default score settings for ingredients");
            }

            IngredientDataMapper.GetData(IngredientKind.Carrot).Score = dishScore.CarrotScore;
            IngredientDataMapper.GetData(IngredientKind.Pepper).Score = dishScore.PepperScore;
            IngredientDataMapper.GetData(IngredientKind.Potato).Score = dishScore.PotatoScore;
            IngredientDataMapper.GetData(IngredientKind.Meat).Score = dishScore.MeatScore;
            IngredientDataMapper.GetData(IngredientKind.Onion).Score = dishScore.OnionScore;
        }

        private void LoadPreviousStateFromMemory()
        {
            Debug.Log("Loading state from memory");
            var json = PlayerPrefs.GetString(GameStateKey, null);

            try
            {
                GameStateAccess.GameState.InitializeFrom(JsonUtility.FromJson<GameState>(json));
            }
            catch
            {
                SaveStateInMemory();
            }
        }

        private void SaveStateInMemory()
        {
            var json = JsonUtility.ToJson(GameStateAccess.GameState);
            Debug.Log($"Saving state into memory.\n{json}");
            PlayerPrefs.SetString(GameStateKey, json);
        }

        private void OnIngredientAddedEvent(IngredientAdded @event)
        {
            var ingredientData = @event.Ingredient.Data;
            GameStateAccess.GameState.currentDish.AddIngredient(ingredientData.Kind);
            PublishStateUpdateEvent(ingredientData.Score);
            HandleFullPanIfIsFull();
            spawners.Release(@event.Ingredient.gameObject);
            SaveStateInMemory();
        }

        private void OnScoreResetRequest(ResetScoreRequest request)
        {
            ResetUtility.ResetAllFields(GameStateAccess.GameState);
            PublishStateUpdateEvent(0);
            SaveStateInMemory();
        }

        private void PublishStateUpdateEvent(int lastIngredientScore)
        {
            EventBus.Publish(
                new ScoreUpdated(
                    GameStateAccess.GameState.currentDish.ScoreSummary.Score,
                    GameStateAccess.GameState.TotalScore,
                    lastIngredientScore
                )
            );
        }

        private void HandleFullPanIfIsFull()
        {
            if (GameStateAccess.GameState.currentDish.ingredients.Count < 5)
                return;

            var dish = new Dish(GameStateAccess.GameState.currentDish.ingredients);
            GameStateAccess.GameState.dishesHistory.Add(dish);
            GameStateAccess.GameState.lastDish = dish;

            var currentDishScore = dish.ScoreSummary.Score;
            var bestDishScore = GameStateAccess.GameState.bestDish.ScoreSummary.Score;
            if (currentDishScore > bestDishScore)
            {
                GameStateAccess.GameState.bestDish = dish;
            }

            EventBus.Publish(new DishCooked(dish));
            GameStateAccess.GameState.currentDish = new Dish(new List<IngredientKind>(5));
        }
    }
}
