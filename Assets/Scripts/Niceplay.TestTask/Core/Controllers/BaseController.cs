using Niceplay.TestTask.Core.Events.Requests;
using Niceplay.TestTask.EventBusSystem;
using UnityEngine;

namespace Niceplay.TestTask.Core.Controllers
{
    public abstract class BaseController : MonoBehaviour
    {
        protected virtual void Awake()
        {
            EventBus.Subscribe<StartControllersRequest>(OnStartController);
        }

        protected abstract void OnStartController(StartControllersRequest request);
    }
}
