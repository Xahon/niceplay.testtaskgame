﻿using Niceplay.TestTask.Pooling;
using UnityEngine;

namespace Niceplay.TestTask.Core
{
    public class IngredientSpawner : MonoBehaviour
    {
        [SerializeField]
        private GameObjectPool pool;

        [SerializeField]
        private Transform objectContainer;

        public GameObjectPool Pool => pool;

        private void OnMouseDown()
        {
            SpawnItem();
        }

        private void SpawnItem()
        {
            var obj = pool.Get();
            var draggable = obj.GetComponentInChildren<MouseDrag>();
            var position = (Vector2) Camera.main.ScreenToWorldPoint(Input.mousePosition);
            obj.transform.position = position;
            obj.transform.SetParent(objectContainer);
            draggable.StartDrag(position);
        }
    }
}
