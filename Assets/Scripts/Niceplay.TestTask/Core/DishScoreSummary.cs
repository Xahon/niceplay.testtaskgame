using System.Collections.Generic;
using System.Linq;
using Niceplay.TestTask.Core.Models;

namespace Niceplay.TestTask.Core
{
    public struct DishScoreSummary
    {
        private static readonly Dictionary<int, float> ComboMultipliers = new Dictionary<int, float>
        {
            {
                1, 1f
            },
            {
                2, 2f
            },
            {
                3, 1.5f
            },
            {
                4, 1.25f
            },
            {
                5, 1f
            },
        };

        public int Score { get; }

        public DishScoreSummary(Dish dish)
        {
            var score = 0;
            foreach (var group in dish.ingredients.GroupBy(i => i))
            {
                var ingredients = group.ToList();
                var sum = ingredients
                    .Select(i => IngredientDataMapper.GetData(i).Score)
                    .Sum();

                score += (int) (sum * ComboMultipliers[ingredients.Count]);
            }

            Score = score;
        }
    }
}
