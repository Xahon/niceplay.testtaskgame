using System;
using Niceplay.TestTask.Core.Settings;
using UnityEngine;

namespace Niceplay.TestTask.Core
{
    public class IngredientDataMapper : Singleton<IngredientDataMapper>
    {
        [SerializeField]
        private IngredientData meatData;

        [SerializeField]
        private IngredientData onionData;

        [SerializeField]
        private IngredientData carrotData;

        [SerializeField]
        private IngredientData pepperData;

        [SerializeField]
        private IngredientData potatoData;

        public static IngredientData GetData(IngredientKind kind)
        {
            switch (kind)
            {
                case IngredientKind.Carrot:
                    return Instance.carrotData;
                case IngredientKind.Meat:
                    return Instance.meatData;
                case IngredientKind.Potato:
                    return Instance.potatoData;
                case IngredientKind.Pepper:
                    return Instance.pepperData;
                case IngredientKind.Onion:
                    return Instance.onionData;
                default:
                    throw new ArgumentOutOfRangeException(nameof(kind), kind, null);
            }
        }
    }
}
