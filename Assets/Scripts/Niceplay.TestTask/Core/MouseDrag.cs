using System.Collections;
using Niceplay.TestTask.Core.Settings;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using MathUtils = Niceplay.TestTask.Utils.MathUtils;

namespace Niceplay.TestTask.Core
{
    [RequireComponent(typeof(Collider2D))]
    public class MouseDrag : MonoBehaviour
    {
        [SerializeField]
        private FloatValue attraction;

        [SerializeField]
        private FloatValue gravityScale;

        [SerializeField]
        private new Rigidbody2D rigidbody;

        private Collider2D _collider;
        private Coroutine _dragCoroutine;

        private void Awake()
        {
            _collider = GetComponent<Collider2D>();
        }

        private void OnDisable()
        {
            StopDrag();
        }

        private void Update()
        {
            var mouseButtonState = Input.GetMouseButton((int) MouseButton.LeftMouse);
            if (mouseButtonState && _dragCoroutine == null)
            {
                var dragPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                if (!_collider.OverlapPoint(dragPoint))
                    return;

                StartDrag(dragPoint);
            }
            else if (!mouseButtonState && _dragCoroutine != null)
            {
                StopDrag();
            }
        }

        public void StartDrag(Vector3 dragPoint)
        {
            rigidbody.gravityScale = 0f;
            _dragCoroutine = StartCoroutine(DragCoroutine(dragPoint));
        }

        private void StopDrag()
        {
            if (_dragCoroutine != null)
            {
                StopCoroutine(_dragCoroutine);
                _dragCoroutine = null;
            }

            rigidbody.gravityScale = gravityScale.Value;
            rigidbody.centerOfMass = Vector2.zero;
        }

        private IEnumerator DragCoroutine(Vector3 dragPoint)
        {
            var t = transform;
            var relDragPoint = t.InverseTransformPoint(dragPoint);
            var lastForce = Vector2.zero;

            while (true)
            {
                Vector2 dragPointWorld = t.TransformPoint(relDragPoint);

                Vector2 cursorPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Vector2 pos = t.position;

                var distance = Vector2.Distance(cursorPosition, pos);
                var multiplier = Mathf.Lerp(
                    0f, attraction.Value,
                    Mathf.Clamp01(
                        MathUtils.Remap(distance, 0f, 100f, 0f, 1f)
                    )
                );
                var force = (cursorPosition - pos).normalized * multiplier;
                rigidbody.AddForceAtPosition(force, dragPointWorld, ForceMode2D.Force);
                rigidbody.AddForceAtPosition(-lastForce, dragPointWorld, ForceMode2D.Force);
                rigidbody.centerOfMass = -relDragPoint;

                lastForce = force;
                yield return new WaitForFixedUpdate();
            }
        }
    }
}
