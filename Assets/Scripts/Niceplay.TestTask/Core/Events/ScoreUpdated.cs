namespace Niceplay.TestTask.Core.Events
{
    public class ScoreUpdated
    {
        public int CurrentDishScore { get; }
        public int TotalGameScore { get; }
        public int LastIngredientScore { get; }

        public ScoreUpdated(int currentDishScore, int totalGameScore, int lastIngredientScore)
        {
            CurrentDishScore = currentDishScore;
            TotalGameScore = totalGameScore;
            LastIngredientScore = lastIngredientScore;
        }
    }
}
