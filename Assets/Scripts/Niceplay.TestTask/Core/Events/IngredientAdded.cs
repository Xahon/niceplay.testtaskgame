namespace Niceplay.TestTask.Core.Events
{
    public class IngredientAdded
    {
        public Ingredient Ingredient { get; }

        public IngredientAdded(Ingredient ingredient)
        {
            Ingredient = ingredient;
        }
    }
}
