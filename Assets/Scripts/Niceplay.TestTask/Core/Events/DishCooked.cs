using Niceplay.TestTask.Core.Models;

namespace Niceplay.TestTask.Core.Events
{
    public class DishCooked
    {
        public Dish Dish { get; }

        public DishCooked(Dish dish)
        {
            Dish = dish;
        }
    }
}
