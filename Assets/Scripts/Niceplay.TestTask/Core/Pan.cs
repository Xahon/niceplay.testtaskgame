using Niceplay.TestTask.Core.Events;
using Niceplay.TestTask.EventBusSystem;
using UnityEngine;

namespace Niceplay.TestTask.Core
{
    [RequireComponent(typeof(Collider2D))]
    public class Pan : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            var ingredient = other.gameObject.GetComponentInParent<Ingredient>();
            if (!ingredient)
                return;

            HandleIngredientAdded(ingredient);
        }

        private void HandleIngredientAdded(Ingredient ingredient)
        {
            EventBus.Publish(new IngredientAdded(ingredient));
        }
    }
}
