using System;
using System.Collections.Generic;
using System.Linq;
using Niceplay.TestTask.Core.Settings;

namespace Niceplay.TestTask.Core.Models
{
    [Serializable]
    public struct Dish
    {
        public List<IngredientKind> ingredients;

        public DishScoreSummary ScoreSummary => new DishScoreSummary(this);
        public bool IsFull => ingredients.Count == 5;

        public Dish(IEnumerable<IngredientKind> ingredients)
        {
            this.ingredients = ingredients.ToList();
        }

        public void AddIngredient(IngredientKind ingredient)
        {
            if (ingredients.Count >= 5)
                throw new OverflowException();

            ingredients.Add(ingredient);
        }

        public override int GetHashCode()
        {
            var sorted = ingredients.OrderBy(kind => (int) kind);
            return sorted.Aggregate(7, (hash, kind) => hash * 13 + (int) kind);
        }

        public override string ToString()
        {
            return $"Dish ({string.Join(", ", ingredients.Select(i => i.ToString()))})";
        }
    }
}
