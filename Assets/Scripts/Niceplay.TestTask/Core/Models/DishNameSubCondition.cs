using System;
using Niceplay.TestTask.Core.Settings;
using UnityEngine;

namespace Niceplay.TestTask.Core.Models
{
    [Serializable]
    public class DishNameSubCondition
    {
        public enum BooleanOperation
        {
            And,
            Or
        }

        public enum EqualityOperation
        {
            Eq,
            Gt,
            Gte,
            Lt,
            Lte
        }

        [SerializeField]
        private IngredientKind ingredient;

        [SerializeField]
        private EqualityOperation op;

        [SerializeField]
        private int count;

        [SerializeField]
        private BooleanOperation bindWithPrevious;

        public IngredientKind Ingredient => ingredient;
        public EqualityOperation Op => op;
        public int Count => count;
        public BooleanOperation BindWithPrevious => bindWithPrevious;

        public DishNameSubCondition(IngredientKind ingredient, EqualityOperation op, int count, BooleanOperation bindWithPrevious)
        {
            this.ingredient = ingredient;
            this.op = op;
            this.count = count;
            this.bindWithPrevious = bindWithPrevious;
        }
    }
}
