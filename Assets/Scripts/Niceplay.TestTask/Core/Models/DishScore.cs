using System;

namespace Niceplay.TestTask.Core.Models
{
    [Serializable]
    public class DishScore
    {
        public int MeatScore = 50;
        public int OnionScore = 40;
        public int PepperScore = 30;
        public int CarrotScore = 20;
        public int PotatoScore = 10;
    }
}
