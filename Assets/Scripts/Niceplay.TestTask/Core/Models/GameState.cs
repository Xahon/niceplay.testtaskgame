using System;
using System.Collections.Generic;
using System.Linq;
using Niceplay.TestTask.Utils;

namespace Niceplay.TestTask.Core.Models
{
    public static class GameStateAccess
    {
        private static GameState _gameState;

        public static GameState GameState
        {
            get
            {
                if (_gameState == null)
                {
                    _gameState = new GameState();
                    ResetUtility.ResetAllFields(_gameState);
                }

                return _gameState;
            }
        }
    }

    [Serializable]
    public class GameState
    {
        public Dish currentDish;
        public Dish lastDish;
        public List<Dish> dishesHistory;
        public Dish bestDish;

        public int TotalScore =>
            dishesHistory
                .Concat(new[]
                {
                    currentDish
                })
                .Sum(d => d.ScoreSummary.Score);

        public void InitializeFrom(GameState other)
        {
            currentDish = other.currentDish;
            lastDish = other.lastDish;
            dishesHistory = other.dishesHistory;
            bestDish = other.bestDish;
        }
    }
}
