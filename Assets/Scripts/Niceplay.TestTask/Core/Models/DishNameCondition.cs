using System;
using System.Collections.Generic;
using System.Linq;
using Niceplay.TestTask.Core.Settings;
using UnityEngine;

namespace Niceplay.TestTask.Core.Models
{
    [Serializable]
    public class DishNameCondition
    {
        [SerializeField]
        private List<DishNameSubCondition> subConditions;

        [SerializeField]
        private string resolution;

        public List<DishNameSubCondition> SubConditions => subConditions;
        public string Resolution => resolution;

        public DishNameCondition(List<DishNameSubCondition> subConditions, string resolution)
        {
            this.subConditions = subConditions;
            this.resolution = resolution;
        }

        public bool IsMet(IEnumerable<IngredientKind> ingredients)
        {
            var ingredientKindsArray = ingredients as IngredientKind[] ?? ingredients.ToArray();
            var previousCondition = true;

            for (var i = 0; i < SubConditions.Count; i++)
            {
                var subCondition = SubConditions[i];
                var count = subCondition.Count;
                var actualCount = ingredientKindsArray.Count(ing => ing == subCondition.Ingredient);

                var result = GetOperationResult(actualCount, count, subCondition.Op);
                var booleanOp = i == 0 ? DishNameSubCondition.BooleanOperation.And : subCondition.BindWithPrevious;
                if (!GetBooleanResult(previousCondition, result, booleanOp))
                    return false;
                previousCondition = result;
            }

            return true;
        }

        private bool GetBooleanResult(bool cond1, bool cond2, DishNameSubCondition.BooleanOperation booleanOperation)
        {
            switch (booleanOperation)
            {
                case DishNameSubCondition.BooleanOperation.And:
                    return cond1 && cond2;
                case DishNameSubCondition.BooleanOperation.Or:
                    return cond1 || cond2;
                default:
                    return false;
            }
        }

        private bool GetOperationResult(int actualCount, int count, DishNameSubCondition.EqualityOperation subConditionOp)
        {
            switch (subConditionOp)
            {
                case DishNameSubCondition.EqualityOperation.Eq:
                    return actualCount == count;
                case DishNameSubCondition.EqualityOperation.Gt:
                    return actualCount > count;
                case DishNameSubCondition.EqualityOperation.Gte:
                    return actualCount >= count;
                case DishNameSubCondition.EqualityOperation.Lt:
                    return actualCount < count;
                case DishNameSubCondition.EqualityOperation.Lte:
                    return actualCount <= count;
                default:
                    return false;
            }
        }
    }
}
