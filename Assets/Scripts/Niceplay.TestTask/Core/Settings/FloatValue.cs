using UnityEngine;

namespace Niceplay.TestTask.Core.Settings
{
    [CreateAssetMenu(menuName = "Float Value", fileName = "FloatValue")]
    public class FloatValue : ScriptableObject
    {
        [SerializeField]
        private float value;

        public float Value => value;
    }
}
