using UnityEngine;

namespace Niceplay.TestTask.Core.Settings
{
    [CreateAssetMenu(menuName = "Ingredient Data", fileName = "Ingredient")]
    public class IngredientData : ScriptableObject
    {
        [SerializeField]
        private IngredientKind kind;

        [SerializeField]
        private int score;

        [SerializeField]
        private Sprite icon;

        public IngredientKind Kind => kind;
        public int Score
        {
            get => score;
            set => score = value;
        }

        public Sprite Icon => icon;
    }

    public enum IngredientKind
    {
        Carrot,
        Meat,
        Potato,
        Pepper,
        Onion
    }
}
