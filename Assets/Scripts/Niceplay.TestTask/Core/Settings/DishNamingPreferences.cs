using System.Collections.Generic;
using System.Linq;
using System.Text;
using Niceplay.TestTask.Core.Models;
using UnityEngine;

namespace Niceplay.TestTask.Core.Settings
{
    [CreateAssetMenu(menuName = "Dish Naming Preferences", fileName = "DishNamingPreferences")]
    public class DishNamingPreferences : ScriptableObject
    {
        [SerializeField]
        private List<DishNameCondition> conditions;

        [SerializeField]
        private string otherwise;

        public List<DishNameCondition> Conditions => conditions;
        public string Otherwise => otherwise;

        public string GetDishName(Dish dish)
        {
            if (!dish.IsFull)
                return "-";

            foreach (var condition in Conditions)
            {
                if (condition.IsMet(dish.ingredients))
                    return condition.Resolution;
            }

            return Otherwise;
        }

        public string GetDishIngredientsString(Dish dish)
        {
            if (!dish.IsFull)
                return "";

            var groups = dish.ingredients.GroupBy(i => i);
            var sb = new StringBuilder("(");
            sb.Append(string.Join(", ", groups.Select(g => $"{g.Key.ToString()} {g.Count()}")));
            sb.Append(")");
            sb.Append($" [{dish.ScoreSummary.Score}]");
            return sb.ToString();
        }
    }
}
