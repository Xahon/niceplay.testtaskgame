using Niceplay.TestTask.Core.Settings;
using UnityEngine;

namespace Niceplay.TestTask.Core
{
    [DisallowMultipleComponent]
    public class Ingredient : MonoBehaviour
    {
        [SerializeField]
        private IngredientData data;

        public IngredientData Data => data;
    }
}
