using System;
using System.Collections.Generic;

namespace Niceplay.TestTask.EventBusSystem
{
    public static class EventBus
    {
        public class Subscription : IDisposable, IEquatable<Subscription>
        {
            public Type Type { get; }
            public object Action { get; }
            public bool IsFreed { get; private set; }
            private Action<Subscription> OnSubscriptionDisposed { get; }

            public Subscription(Type type, object action, Action<Subscription> onSubscriptionDisposed)
            {
                Type = type;
                Action = action;
                OnSubscriptionDisposed = onSubscriptionDisposed;
            }

            public void Dispose()
            {
                OnSubscriptionDisposed.Invoke(this);
                IsFreed = true;
            }

            #region Equality

            public bool Equals(Subscription other)
            {
                if (ReferenceEquals(null, other))
                    return false;
                return ReferenceEquals(this, other) || Equals(Action, other.Action);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj))
                    return false;
                if (ReferenceEquals(this, obj))
                    return true;
                return obj.GetType() == GetType() && Equals((Subscription) obj);
            }

            public override int GetHashCode()
            {
                return Action != null ? Action.GetHashCode() : 0;
            }

            #endregion
        }

        private class EventBusWorker
        {
            private readonly Dictionary<Type, List<Subscription>> _subscriptions = new Dictionary<Type, List<Subscription>>(10);

            public Subscription AddSubscription<T>(Action<T> action)
            {
                var subscription = new Subscription(typeof(T), action, OnSubscriptionDisposed);
                if (!_subscriptions.ContainsKey(subscription.Type))
                    _subscriptions[subscription.Type] = new List<Subscription>(10);
                _subscriptions[subscription.Type].Add(subscription);
                return subscription;
            }

            public void RemoveSubscription(Subscription subscription)
            {
                if (subscription == null || subscription.IsFreed)
                    return;
                if (!_subscriptions.ContainsKey(subscription.Type))
                    return;
                _subscriptions[subscription.Type].Remove(subscription);
                subscription.Dispose();
            }

            public void OnSubscriptionDisposed(Subscription subscription)
            {
                _subscriptions[subscription.Type].Remove(subscription);
            }

            public void Publish<T>(T data)
            {
                var type = typeof(T);
                if (!_subscriptions.ContainsKey(type))
                    return;
                foreach (var subscription in _subscriptions[type])
                {
                    ((Action<T>) subscription.Action)(data);
                }
            }
        }

        private static EventBusWorker _instance;

        private static EventBusWorker Instance
        {
            get
            {
                lock (InstanceLock)
                    return _instance ?? (_instance = new EventBusWorker());
            }
        }

        private static readonly object InstanceLock = new object();

        public static void Publish<T>(T data)
        {
            Instance.Publish(data);
        }

        public static Subscription Subscribe<T>(Action<T> action)
        {
            return Instance.AddSubscription(action);
        }

        public static void Unsubscribe(Subscription subscription)
        {
            Instance.RemoveSubscription(subscription);
        }
    }
}
