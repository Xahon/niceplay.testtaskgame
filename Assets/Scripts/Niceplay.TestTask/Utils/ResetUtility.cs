using System;
using System.Collections.Generic;
using System.Reflection;

namespace Niceplay.TestTask.Utils
{
    public static class ResetUtility
    {
        public static void ResetAllFields(object obj)
        {
            var fields = obj.GetType().GetFields((BindingFlags) 52); // public/non-public/instance

            foreach (var fieldInfo in fields)
            {
                var type = fieldInfo.FieldType;
                var isGeneric = type.IsGenericType;
                var genericType = (Type) null;
                if (isGeneric)
                    genericType = type.GetGenericTypeDefinition();

                if (type.IsPrimitive)
                {
                    fieldInfo.SetValue(obj, Activator.CreateInstance(type));
                    continue;
                }

                if (type == typeof(string))
                {
                    fieldInfo.SetValue(obj, "");
                    continue;
                }

                if (type.IsArray)
                {
                    fieldInfo.SetValue(obj, Array.CreateInstance(type, 0));
                    continue;
                }

                if (genericType == typeof(List<>))
                {
                    fieldInfo.SetValue(obj, Activator.CreateInstance(typeof(List<>).MakeGenericType(type.GenericTypeArguments[0])));
                    continue;
                }

                if (type.IsValueType)
                {
                    var value = fieldInfo.GetValue(obj);
                    ResetAllFields(value);
                    fieldInfo.SetValue(obj, value);
                }

                if (type.IsClass)
                {
                    ResetAllFields(fieldInfo.GetValue(obj));
                }
            }
        }
    }
}
