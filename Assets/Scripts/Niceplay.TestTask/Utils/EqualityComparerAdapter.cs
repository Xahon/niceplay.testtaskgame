using System;
using System.Collections.Generic;

namespace Niceplay.TestTask.Utils
{
    public class EqualityComparerAdapter<T> : IEqualityComparer<T>
    {
        private readonly Func<T, T, bool> _comparisonFunc;
        private readonly Func<T, int> _hashCodeFunc;

        public EqualityComparerAdapter(Func<T, T, bool> comparisonFunc) : this(comparisonFunc, arg => arg.GetHashCode())
        {
        }

        public EqualityComparerAdapter(Func<T, T, bool> comparisonFunc, Func<T, int> hashCodeFunc)
        {
            _comparisonFunc = comparisonFunc;
            _hashCodeFunc = hashCodeFunc;
        }

        public bool Equals(T x, T y)
        {
            return _comparisonFunc(x, y);
        }

        public int GetHashCode(T obj)
        {
            return _hashCodeFunc(obj);
        }
    }
}
