using Niceplay.TestTask.Core;
using Niceplay.TestTask.Core.Models;
using UnityEditor;
using UnityEngine;

namespace Niceplay.TestTask.Editor
{
    [CustomPropertyDrawer(typeof(DishNameSubCondition))]
    internal class DishNameSubConditionEditor : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            // Calculate rects
            var rowHeight = position.height / 2;

            var bindRect = new Rect(position.x, position.y, 50, rowHeight);
            var typeRect = new Rect(position.x, position.y + rowHeight, 90, rowHeight);
            var opRect = new Rect(position.x + 90, position.y + rowHeight, 50, rowHeight);
            var countRect = new Rect(position.x + 90 + 50, position.y + rowHeight, 60, rowHeight);

            EditorGUI.PropertyField(bindRect, property.FindPropertyRelative("bindWithPrevious"), GUIContent.none);
            EditorGUI.PropertyField(typeRect, property.FindPropertyRelative("ingredient"), GUIContent.none);
            EditorGUI.PropertyField(opRect, property.FindPropertyRelative("op"), GUIContent.none);
            EditorGUI.PropertyField(countRect, property.FindPropertyRelative("count"), GUIContent.none);

            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) * 2f;
        }
    }
}
