﻿using System.Collections.Generic;
using System.Linq;
using Niceplay.TestTask.Extensions;
using UnityEngine;

namespace Niceplay.TestTask.Pooling
{
    public class GameObjectPool : MonoBehaviour
    {
        [SerializeField]
        private int initialCountOfEach = 3;

        [SerializeField]
        private int extendCount = 3;

        [SerializeField]
        private GameObject prefab;

        private List<GameObject> _pooled;
        private HashSet<int> _inUse;

        private void Awake()
        {
            _pooled = new List<GameObject>(initialCountOfEach);
            _inUse = new HashSet<int>();

            ExtendPool(initialCountOfEach);
        }

        public void ExtendPool(int count)
        {
            Debug.Log($"Extending '{name}' group for {count} objects");
            _pooled.AddRange(
                Enumerable
                    .Range(0, count)
                    .Select(_ => CreateObject())
            );
        }

        public GameObject Get()
        {
            if (!_pooled.Any())
                ExtendPool(extendCount);
            var obj = BorrowObject();
            if (obj.Value() == null)
            {
                Debug.LogWarning("A pooled object was destroyed before, extending pool");
                ExtendPool(extendCount);
                obj = BorrowObject();
            }

            obj.SetActive(true);
            obj.transform.SetParent(null);

            Debug.Log($"Object '{obj.GetInstanceID()}' was taken from pool ({name})");
            return obj;
        }

        public bool Release(GameObject obj)
        {
            if (obj.Value() == null)
                return false;

            var instanceId = obj.GetInstanceID();
            if (!_inUse.Contains(instanceId))
                return false;

            _inUse.Remove(instanceId);
            _pooled.Add(obj);
            HideObject(obj);

            Debug.Log($"Object '{obj.GetInstanceID()}' was returned into pool ({name})");
            return true;
        }

        private GameObject BorrowObject()
        {
            var obj = _pooled[0];
            _pooled.RemoveAt(0);
            var instanceId = obj.GetInstanceID();
            if (!_inUse.Contains(instanceId))
                _inUse.Add(instanceId);
            return obj;
        }

        private GameObject CreateObject()
        {
            var cmp = Instantiate(prefab);
            var obj = cmp.gameObject;
            HideObject(obj);
            return obj;
        }

        private void HideObject(GameObject obj)
        {
            obj.transform.SetParent(transform);
            obj.SetActive(false);
        }
    }
}
